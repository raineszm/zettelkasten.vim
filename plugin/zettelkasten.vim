if exists("g:loaded_zettelkasten")
    finish
endif

let g:loaded_zettelkasten = 1

if !exists("g:zettelkasten_id_format")
    let g:zettelkasten_id_format = "%Y%m%d%H%M%S"
endif

if !exists("g:zettelkasten_prefix_id")
    let g:zettelkasten_prefix_id = 0
endif

if !exists("g:zettelkasten_today_format")
    let g:zettelkasten_today_format = "%Y-%m-%d"
endif

if !exists("g:zettelkasten_base_path")
    let g:zettelkasten_base_path = expand('~/Documents/Zettel')
endif

if !exists("g:zettelkasten_vaults")
    let g:zettelkasten_vaults = {
                \'default': g:zettelkasten_base_path
                \ }
endif

if !exists("g:zettelkasten_today_path")
    let g:zettelkasten_today_path = "Journal"
endif

if !exists("g:zettelkasten_zettel_path")
    let g:zettelkasten_zettel_path = "Zettel"
endif

if !exists('g:unite_source_alias_aliases')
    let g:unite_source_alias_aliases = {}
end

let g:unite_source_alias_aliases['zettel'] = {
            \'source': 'file_rec',
            \'description': 'entries in the zettlekasten vault'
            \}

let g:unite_source_alias_aliases['backlinks'] = {
            \'source': 'vimgrep',
            \'description': 'backlinks to the current file'
            \}

command ZettelToday call zettelkasten#OpenToday()
command -nargs=? ZettelNew call zettelkasten#NewZettel(<f-args>)
command -nargs=? ZettelOpen call zettelkasten#OpenNote(<f-args>)
command ZettelLink call zettelkasten#GetLink()
command ZettelFollow call zettelkasten#FollowLink()
command ZettelNextLink call zettelkasten#NextLink()
command ZettelPreviousLink call zettelkasten#NextLink(1)
command ZettelBackLinks call zettelkasten#BackLinks()
command ZettelVault Unite vaults -no-start-insert -no-empty

noremap <Plug>(zettelkasten_open_today) :<C-u>ZettelToday<CR>
noremap <Plug>(zettelkasten_insert_link) :<C-u>ZettelLink<CR>
noremap <Plug>(zettelkasten_follow_link) :<C-u>ZettelFollow<CR>
noremap <Plug>(zettelkasten_next_link) :<C-u>ZettelNextLink<CR>
noremap <Plug>(zettelkasten_previous_link) :<C-u>ZettelPreviousLink<CR>
noremap <Plug>(zettelkasten_find_file) :<C-u>ZettelOpen<CR>
noremap <Plug>(zettelkasten_new_zettel) :<C-u>ZettelNew<CR>
noremap <Plug>(zettelkasten_backlinks) :<C-u>ZettelBackLinks<CR>
noremap <Plug>(zettelkasten_vault) :<C-u>ZettelVault<CR>


if !exists('g:zettelkasten_no_mappings') || !g:zettelkasen_no_mappings
    map <silent> <Leader>zt <Plug>(zettelkasten_open_today)
    map <silent> <Leader>zl <Plug>(zettelkasten_insert_link)
    map <silent> <Leader>zf <Plug>(zettelkasten_find_file)
    map <silent> <Leader>zn <Plug>(zettelkasten_new_zettel)
    map <silent> <Leader>zv <Plug>(zettelkasten_vault)
endif

if !exists('g:zettelkasten_no_mappings') || !g:zettelkasen_no_mappings
    augroup zettel
        autocmd!
        autocmd FileType markdown map <buffer> <silent> <LocalLeader>f <Plug>(zettelkasten_follow_link)
        autocmd FileType markdown map <buffer> <silent> <LocalLeader>b <Plug>(zettelkasten_backlinks)
        autocmd FileType markdown call zettelkasten#BindEnter()
        autocmd Syntax markdown call zettelkasten#HighlightLinks()
    augroup END
endif


