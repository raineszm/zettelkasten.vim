let s:module_path = expand('<sfile>', ':p:h')
function! s:AddPythonModuleToPath()
pyx << END
from pathlib import Path
import sys
module_path = Path(vim.eval('s:module_path'))
lib_path = module_path.absolute().parents[1] / 'python'
sys.path.append(str(lib_path))
import zettelkasten
END
endf
call s:AddPythonModuleToPath()

" Define settings on unite sources
call unite#custom#source('zettel', 'matchers', ['matcher_default', 'matcher_filter_markdown'])
call unite#custom#source('zettel', 'converters', ['converter_abbr_zettel_name'])

call unite#custom#profile('source/zettel', 'context', {
                \ 'ignorecase': 1
            \})

call unite#custom#source('backlinks', 'matchers', ['matcher_default', 'matcher_filter_markdown'])
call unite#custom#source('backlinks', 'converters', ['converter_backlinks'])

call unite#custom#profile('source/backlinks', 'context', {
                \ 'ignorecase': 1,
                \ 'start_insert': 0,
            \})

function! zettelkasten#TodayPath()
    return g:zettelkasten_base_path . "/" . g:zettelkasten_today_path . "/" . strftime(g:zettelkasten_today_format) . ".md"
endfunction

function! zettelkasten#OpenToday()
    execute ":e " . zettelkasten#TodayPath()
endfunction

function! zettelkasten#NewId()
    return strftime(g:zettelkasten_id_format)
endfunction

function! zettelkasten#ZettelPath(title)
    let l:path = g:zettelkasten_base_path .. "/" . g:zettelkasten_zettel_path .. "/"
    if g:zettelkasten_prefix_id
        let l:path ..= zettelkasten#NewId() .. " "
    endif
    let l:path ..= a:title .. ".md"
    return l:path
endfunction

function! zettelkasten#NewZettel(title='')
    if a:title is# ''
        call inputsave()
        let l:title = input("Note title> ")
        call inputrestore()
    else
        let l:title = a:title
    endif
    let l:path = zettelkasten#ZettelPath(l:title)
    execute "edit " . l:path
endfunction

function! s:RelativePath(file, reference)
    return pyxeval('zettelkasten.relative_path(vim.eval("a:file"), vim.eval("a:reference"))')
endfunction

function! zettelkasten#RelativeToBase(file)
    return s:RelativePath(a:file, g:zettelkasten_base_path)
endf

function! zettelkasten#IsZettel(file)
    return pyxeval('zettelkasten.is_zettel(vim.eval("a:file"))')
endfunction

function! zettelkasten#InsertRelativeLink(fpath)
    let l:link_text = "[[" . s:RelativePath(a:fpath, expand('%:p:h')) . "]]"
    execute "normal \<Esc>a\<C-r>\<C-r>=l:link_text\<CR>"
endfunction

function! zettelkasten#InsertLink(fpath)
    let l:link_text = "[[" . fnamemodify(a:fpath, ':t:r') . "]]"
    execute "normal \<Esc>a\<C-r>\<C-r>=l:link_text\<CR>"
endfunction


let unite_wikilink = { "description": "insert wiki link" }
function! unite_wikilink.func(candidate) abort
    let l:path = a:candidate.action__path
    call zettelkasten#InsertLink(l:path)
endfunction
call unite#custom#action('file', 'wikilink', unite_wikilink)
unlet unite_wikilink

function! zettelkasten#GetLink()
    call unite#start(['zettel'], {
                \ 'default_action' : 'wikilink',
                \ 'path': g:zettelkasten_base_path
                \})
endfunction

function! zettelkasten#BackLinks()
    let l:name = expand('%:t:r')
    call unite#start([['backlinks', g:zettelkasten_base_path, l:name]],
                \{
                \ 'path': g:zettelkasten_base_path
                \})
endf

function! zettelkasten#OpenNote(search='', start_insert=1)
    call unite#start(['zettel'],
                \ {
                \ 'input': a:search,
                \ 'path': g:zettelkasten_base_path,
                \ 'start_insert': a:start_insert,
                \ 'immediately': 1
                \})
endf

function! zettelkasten#FollowLink()
    let l:saved = @@
    normal! yi[
    echom @@
    let l:title = split(@@, '\v\|')[0]
    echom l:title
    let @@ = l:saved
    call zettelkasten#OpenNote(l:title, 0)
endfunc

function! zettelkasten#NextLink(backward=0)
    let l:options = 'ze'
    if a:backward
        let l:options ..= 'b'
    endif
    call search('\v\[\[', l:options)
endf



function! zettelkasten#BindEnter()
    if exists('g:zettelkasten_no_bind_enter') && g:zettelkasten_no_bind_enter
        return
    endif

    let l:bind = zettelkasten#IsZettel(expand('%'))

    if l:bind
        nmap <buffer> <silent> <Enter> <Plug>(zettelkasten_follow_link)
        nmap <buffer> <silent> <Tab> <Plug>(zettelkasten_next_link)
        nmap <buffer> <silent> <S-Tab> <Plug>(zettelkasten_previous_link)
    endif
endf

function! zettelkasten#HighlightLinks()
    let l:highlight = zettelkasten#IsZettel(expand('%'))

    if l:highlight
        syn region mkdWikiLink matchgroup=mkdDelimiter start="\[\["   end="\]\]" contained oneline
        syn cluster mkdNonListItem add=mkdWikiLink
        highlight link mkdWikiLink htmlLink
    endif
endf
