let s:save_cpo = &cpo
set cpo&vim

function! unite#filters#converter_abbr_zettel_name#define() abort "{{{
    return s:converter
endfunction"}}}

let s:converter = {
            \ 'name' : 'converter_abbr_zettel_name',
            \ 'description' : 'replace abbr with the name of the zettel',
            \}

function! s:SimplifyAbbr(key, val)
    let a:val['abbr'] = fnamemodify(a:val['word'], ':t:r')
    return a:val
endfunc

function! s:converter.filter(candidates, context) abort "{{{
    return map(a:candidates, function('s:SimplifyAbbr'))
endfunction"}}}

let &cpo = s:save_cpo
unlet s:save_cpo
