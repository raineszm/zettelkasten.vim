let s:save_cpo = &cpo
set cpo&vim

function! unite#filters#matcher_filter_markdown#define() abort "{{{
    return s:matcher
endfunction"}}}

let s:matcher = {
            \ 'name' : 'matcher_filter_markdown',
            \ 'description' : 'select only markdown files',
            \}

function! s:IsMarkdown(key, val)
    if !has_key(a:val, 'action__path')
        return 0
    end
    let l:extension = fnamemodify(a:val['action__path'], ':e')
    return l:extension is# 'md'
endf

function! s:matcher.filter(candidates, context) abort "{{{
    return filter(a:candidates, function("s:IsMarkdown"))
endfunction"}}}

let &cpo = s:save_cpo
unlet s:save_cpo
