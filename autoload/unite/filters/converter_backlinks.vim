let s:save_cpo = &cpo
set cpo&vim

function! unite#filters#converter_backlinks#define() abort "{{{
    return s:converter
endfunction"}}}

let s:converter = {
            \ 'name' : 'converter_backlinks',
            \ 'description' : 'format backlink display',
            \}

function! s:FormatBacklink(key, val)
    let a:val.word = printf('%s:%s:%s',
                \     fnamemodify(a:val.action__path, ':t:r'),
                \ a:val.action__line, a:val.action__text)
    return a:val
endfunc

function! s:converter.filter(candidates, context) abort "{{{
    return map(a:candidates, function('s:FormatBacklink'))
endfunction"}}}

let &cpo = s:save_cpo
unlet s:save_cpo
