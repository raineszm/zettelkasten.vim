let s:save_cpo = &cpo
set cpo&vim

function! unite#sources#vaults#define() abort "{{{
    return s:source
endfunction"}}}

let s:source = {
            \ 'name' : 'vaults',
            \ 'description' : 'candidates for vault folder',
            \ 'default_action' : 'select',
            \ 'default_kind' : 'vault',
            \}

function! s:candidate(key, val)
    let l:candidate = {
                \ 'word' : a:key,
                \ 'action__path' : a:val
                \}
    return l:candidate
endfunction

function! s:source.gather_candidates(args, context) abort "{{{
    let l:vaults = filter(g:zettelkasten_vaults,
                \        '(empty(a:args) ||
                \            index(a:args, v:key) >= 0)')
    return values(map(copy(l:vaults), function('s:candidate')))
endfunction"}}}

let &cpo = s:save_cpo
unlet s:save_cpo
