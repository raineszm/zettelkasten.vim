let s:save_cpo = &cpo
set cpo&vim

function! unite#kinds#vault#define() abort "{{{
  return s:kind
endfunction"}}}

let s:kind = {
      \ 'name' : 'vault',
      \ 'default_action' : 'select',
      \ 'action_table': {},
      \}

" Actions "{{{
let s:kind.action_table.select= {
      \ 'description' : 'select vault',
      \ 'is_quit' : 1,
      \ }

function! s:kind.action_table.select.func(candidate) abort "{{{
    let g:zettelkasten_base_path = a:candidate.action__path
endfunction"}}}

let &cpo = s:save_cpo
unlet s:save_cpo
