from pathlib import Path
import os.path
import vim

def is_zettel(file):
    file = Path(vim.eval('a:file')).absolute().resolve()
    zettel = Path(vim.vars['zettelkasten_base_path'].decode()).absolute().resolve()
    return zettel in file.parents


def relative_path(file, reference):
    fpath = os.path.realpath(os.path.abspath(file))
    refpath = os.path.realpath(os.path.abspath(reference))
    return os.path.relpath(fpath, refpath)
